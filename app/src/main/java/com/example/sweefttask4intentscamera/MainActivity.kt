package com.example.sweefttask4intentscamera

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import com.example.sweefttask4intentscamera.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initListener()
    }

    private fun initListener() {
        val requestCamera = registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (it) {
                initIntent()
                Toast.makeText(this, GRANTED , Toast.LENGTH_SHORT).show()
            }else {
                Toast.makeText(this, NOT_GRANTED , Toast.LENGTH_SHORT).show()
            }
        }
        binding.buttonOpenCamera.setOnClickListener {
            requestCamera.launch(android.Manifest.permission.CAMERA)
        }
    }

    private fun initIntent() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (intent.resolveActivity(this.packageManager) != null) {
            startActivityForResult(intent, 1)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            val thumbnail: Bitmap? = data?.getParcelableExtra(DATA)
            binding.imageView.setImageBitmap(thumbnail)
        }else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    companion object {
        const val GRANTED = "Permission Granted"
        const val NOT_GRANTED = "Permission Not Granted"
        const val DATA = "data"
    }

}